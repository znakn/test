<?php

include_once('Currency.php');
include_once('functions.php');

$baseCurrencyCode = 'USD';


if (isset($_POST['currencyCode']) && ($_POST['currencyCode']) && isset($_POST['currencyCount']) && ($_POST['currencyCount'])) {
    $currencyCode = $_POST['currencyCode'];
    $currencyCount = abs((integer)$_POST['currencyCount']);

    if (!$currencyCount) {
        echo json_encode(['error' => '-2', 'message' => 'Error request information', 'amount' => '0']);
        exit;
    }

    $currencyList = getCurrencyList();

    $baseCurrencyRate = 0;
    $currencyRate = 0;

    if (is_array($currencyList) && (!empty($currencyList))) {
        /** @var Currency $currency */
        foreach ($currencyList as $currency) {
            if ($baseCurrencyCode == $currency->getCode()) {
                $baseCurrencyRate = $currency->getRate();
            }
            if ($currencyCode == $currency->getCode()) {
                $currencyRate = $currency->getRate();
            }
            if ($currencyRate && $baseCurrencyRate) break;
        }

        $amount = round($baseCurrencyRate * $currencyCount / $currencyRate, 2);
        echo json_encode(['error' => '0', 'message' => 'OK', 'amount' => $amount.' '.$currencyCode]);
        exit;


    } else {
        echo json_encode(['error' => '-1', 'message' => 'Error read information from currency server', 'amount' => '0']);
        exit;
    }


} else {
    echo json_encode(['error' => '-2', 'message' => 'Error request information', 'amount' => '0']);
    exit;
}
