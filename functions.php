<?php

include_once('Currency.php');

function ppr($var)
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

function ppre($var)
{
    ppr($var);
    exit;
}


/**
 * @return array Currency
 */
function getCurrencyList()
{
    $path = 'http://bank-ua.com/export/currrate.xml';
    $currencyArray = [];
    $rates = file_get_contents($path);
    if ($rates) {
        $xml = simplexml_load_string($rates);
        foreach ($xml->item as $item) {
            if ((string)$item->name) {
                $rate = round($item->rate / 100, 2);
                $currency = new Currency();
                $currency->setCode((string)$item->char3);
                $currency->setRate($rate);
                $currency->setName((string)$item->name);
                $currencyArray[] = $currency;
            }
        }
    }
    return $currencyArray;
}

