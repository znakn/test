<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Test project</title>
    <!-- Bootstrap -->
    <link href="/vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
</head>
<body>

<?php
include_once('Currency.php');
include_once('functions.php');
?>

<div class="row">
    <div class="col-10 offset-1">
        <?php $currencyArray = getCurrencyList(); ?>
        <?php if (is_array($currencyArray) && (!empty($currencyArray))) : ?>
            <form>
                <div class="form-row">
                    <h3>Convector USD to another currency</h3>
                </div>

                <div class="form-row">
                    <div class="col">
                        <input type="text" id="currencyCount" class="form-control" value="100">
                    </div>
                    <div class="col">
                        <select class="form-control" id="currencyList">

                            <?php foreach ($currencyArray as $currency) : ?>
                                <option value="<?php echo $currency->getCode(); ?>"><?php echo $currency->getCode() . ' ' . $currency->getName() ?></option>
                            <?php endforeach; ?>

                        </select>
                    </div>
                    <div class="col">
                        <input type="button" class="form-control btn-info" id="bConvert" value="Get Rate">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <span>Result</span>
                    </div>
                    <div class="col">
                        <span id="res"></span>
                    </div>
                </div>
            </form>
        <?php else : ?>
            <h3>Error get information from server</h3>
        <?php endif; ?>


    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
<script src="/js/jquery.mask.min.js"></script>
<script src="/js/script.js"></script>
</body>
</html>



