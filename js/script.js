$(function () {

    $('#currencyCount').mask('0000');


    $('#bConvert').click(function () {
        var currencyCode = $('#currencyList option:selected').val();
        var currencyCount = $('#currencyCount').val();
        console.log(currencyCode);
        console.log(currencyCount);
        $.ajax({
            'url': '/convert.php',
            'data': {
                'currencyCode': currencyCode,
                'currencyCount': currencyCount
            },
            'async': false,
            'method': 'POST',

        }).done(function (res) {
            var r = JSON.parse(res);
            if (r.error == '0') {
                $('#res').html(r.amount);
            } else {
                $('#res').html(r.message);
            }
        });

    });

});
